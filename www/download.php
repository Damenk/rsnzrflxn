<?php
//server-side code to save the given drawing in a PNG file
$img = filter_input(INPUT_POST, 'image', FILTER_SANITIZE_URL);
$name = md5(microtime());
$img = str_replace(' ', '+', str_replace('data:image/png;base64,', '', $img));
$data = base64_decode($img);
//create the image png file with the given name
file_put_contents(__DIR__.'/../files/drawings/'. str_replace(' ', '_', $name) .'.png', $data);


ignore_user_abort(true);
set_time_limit(0); // disable the time limit for this script
 

$path = __DIR__."/../files/RSNZRFLXN_SMPLR1.zip"; 
 
$dl_file = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).]|[\.]{2,})", '', $_GET['download_file']); // simple file name validation
$dl_file = filter_var($dl_file, FILTER_SANITIZE_URL); // Remove (more) invalid characters
$fullPath = $path.$dl_file;
 
if ($fd = fopen ($fullPath, "r")) {
	$fsize = filesize($fullPath);
	$path_parts = pathinfo($fullPath);
	$ext = strtolower($path_parts["extension"]);

	header("Content-type: application/octet-stream");
	header("Content-Disposition: filename=\"".$path_parts["basename"]."\"");
	break;

	header("Content-length: $fsize");
	header("Cache-control: private");
	while(!feof($fd)) {
		$buffer = fread($fd, 2048);
		echo $buffer;
	}
	$statsFile = "files/stats.txt";
	$current = file_get_contents($statsFile);
	$current ++;
	file_put_contents($statsFile, $current);
}
fclose ($fd);
$answer = "Viel Spaß damit!";
include("index.php");
exit;
?>