
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="RSNZRFLXN">
    <meta name="author" content="Ele">
    <link rel="shortcut icon" href="public/img/favicon.png" />

    <title>RSNZRFLXN Sampler</title>


    <link href="public/css/bootstrap.min.css" rel="stylesheet">
	<link href="public/css/custom.css" rel="stylesheet">
	<link href="public/css/drawingboard.min.css" rel="stylesheet">
	
	 <script src="public/js/jquery-3.1.0.min.js"></script>
	 <script src="public/js/drawingboard.min.js"></script>
	 <script src="public/js/drawing.js"></script>
  </head>
  <body>
    <div class="container">
		<br>
		<div class="row">
			<div id="drawing-container"  class ="col-md-12" >
					<h1>Du malst uns ein Bild, <br> wir schenken dir den Sampler!</h1>
					<br>
				<form id="drawing-form" class="drawing-form" action="download.php" method="post">
				  <div id="drawing"></div>
				  <input type="hidden" name="image" value="">
				  <br>
				  <a class="btn-oswald" href="#" onclick="$('#drawing-form').submit();">- Fertig, her mit dem Sampler! -</a>
				</form>
			</div>
		</div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="public/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="public/js/bootstrap.min.js"></script>
      </body>
</html>
