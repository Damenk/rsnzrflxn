
$(document).ready(function () {

	var myBoard = new DrawingBoard.Board('drawing', {
		controlsPosition:"center",
	});

	$('.drawing-form').on('submit', function (e) {

		var img = myBoard.getImg();

		var imgInput = (myBoard.blankCanvas == img) ? '' : img;

		$(this).find('input[name=image]').val(imgInput);

		myBoard.clearWebStorage();
	});
	
})
